﻿#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <stdlib.h>

void onMouse(int Event, int x, int y, int flags, void *param);
void SharpeningFilter(int width,int Ymin,int Ymax, int Xmin, int Xmax, unsigned short *Input ,unsigned short *Output);
void freeMemory();
CvPoint startMousePoint, endMousePoint;
CvPoint startMousePoint2, endMousePoint2;
IplImage *src, *srcROI, *binarySrc, *dilnEro, *dst, *contours, *GrayImage, *laplacian, *temp;
IplImage	   *srcROI2,*binarySrc2,*dilnEro2,*dst2,*contours2,*GrayImage2,*laplacian2;
int Mode = 0;
int ROIHeight, ROIWidth;
int ROIHeight2, ROIWidth2;
unsigned short *GrayArray;
unsigned short *tempGrayArray;
int mask[9] = {-1,-1,-1,-1,8,-1,-1,-1,-1};

void SharpeningFilter(int width,int Ymin,int Ymax, int Xmin, int Xmax, unsigned short *Input ,unsigned short *Output)
{
	for(int i = Ymin+1,l=0; i < Ymax ;i++,l++)
	{
		for(int j = Xmin+1,k=0; j<Xmax; j++,k++)
		{
			if(i >= Ymin+2 && i < Ymax -1 && j>=Xmin+2 && j < Xmax -1)
			{
				Output[i*width+j] = (mask[4]*Input[i*width+j]				+
									mask[6]*Input[(i-1)*width+(j-1)]		+
									mask[3]*Input[(i-1)*width+j]			+
									mask[0]*Input[(i-1)*width+(j+1)]			+
									mask[7]*Input[i*width+(j-1)]			+
									mask[1]*Input[i*width+(j+1)]			+
									mask[8]*Input[(i+1)*width+(j-1)]		+
									mask[5]*Input[(i+1)*width+j]			+
									mask[2]*Input[(i+1)*width+(j+1)]);					
			}	
		}
	}
}

int BinarizeImageByOTSU (IplImage * src)  
{   
    assert(src != NULL);  
    
    CvRect rect = cvGetImageROI(src);  
   
    int x = rect.x;  
    int y = rect.y;  
    int width = rect.width;   
    int height = rect.height;  
    int ws = src->widthStep;  
   
    int thresholdValue=1;
    int ihist [256] ;
    int i, j, k,n, n1, n2, Color=0;  
    double m1, m2, sum, csum, fmax, sb;  
    memset (ihist, 0, sizeof (ihist)) ;  
   
    for (i=y;i< y+height;i++)
    {   
        int mul =  i*ws;  
        for (j=x;j<x+width;j++)  
        {   
            //Color=Point (i,j) ;  
            Color = (int)(unsigned char)*(src->imageData + mul+ j);  
            ihist [Color] +=1;  
        }  
    }  
    sum=csum=0.0;  
    n=0;  
    for (k = 0; k <= 255; k++)  
    {   
        sum+= (double) k* (double) ihist [k];
        n +=ihist [k];
    }  

    fmax = - 1.0;  
    n1 = 0;  
    for (k=0;k<255;k++)   
    {  
        n1+=ihist [k] ;  
        if (! n1)  
        {   
            continue;   
        }  
        n2=n- n1;  
        if (n2==0)   
        {  
            break;  
        }  
        csum+= (double) k*ihist [k] ;  
        m1=csum/ n1;  
        m2= (sum- csum) /n2;  
        sb = ( double) n1* ( double) n2* ( m1 - m2) * (m1- m2) ;  
   
        if (sb>fmax)   
        {  
            fmax=sb;  
            thresholdValue=k;  
        }  
    }     
    
	if(thresholdValue<30)
	{
		cvThreshold( src, src ,thresholdValue, 255, CV_THRESH_BINARY );
	}
	else if(thresholdValue > 100) 
	{
		cvThreshold( src, src ,128, 255, CV_THRESH_BINARY );
	}
	else
	{
		cvThreshold(src,src,30,255,CV_THRESH_BINARY); 
	}
	printf("thresholdValue : %d\n",thresholdValue);
	
    return 0;  
}

void doBinaryNSharpen()
{
	BinarizeImageByOTSU(GrayImage);

	for(int i = 0 ; i < src->height ;i++)
	{
		for(int j = 0 ; j < src->width ;j++)
		{
			CvScalar x = cvGet2D(GrayImage,i,j);
			int y = x.val[2] + x.val[1] + x.val[0];
			GrayArray[i*src->width+j]=y;
			tempGrayArray[i*src->width+j]=y;
		}
	}
	
	SharpeningFilter(GrayImage->width,0,GrayImage->height, 0,GrayImage->width, tempGrayArray, GrayArray);

	for(int i = 0 ; i < src->height ;i++)
	{
		for(int j = 0 ; j < src->width ;j++)
		{
			cvSet2D(temp, i,j,CV_RGB(GrayArray[i*src->width+j],GrayArray[i*src->width+j],GrayArray[i*src->width+j]));
		}
	}
	cvCvtColor(temp,laplacian,CV_BGR2GRAY);
}

int main()
{
	int choose;
	int area, count, area2, count2, numOfcontour, numOfcontour2,bigContour = 0,bigContour2 = 0;
	float ratio, ratio2;
	char InputFileName[50];
	char userHitKey;
	bool isHaveCar = false, isHaveCar2 = false;

	CvRect rectROI, rectROI2;
	CvRect AndRect, AndRect2;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvMemStorage* storage2 = cvCreateMemStorage(0);
    CvSeq* contour = 0;
	CvSeq* contour2 = 0;
	CvScalar color = CV_RGB( 255, 0, 0); 
	CvPoint fRECT1, fRECT2, sRECT1, sRECT2;
	CvFont font=cvFont(5,5);

	do
	{
		printf("Enter file name:\n");
		scanf("%s", &InputFileName);
		printf("Two or One parking place?\n");
		scanf("%d", &choose);

		src = cvLoadImage(InputFileName, CV_LOAD_IMAGE_COLOR);
		srcROI = cvLoadImage(InputFileName, 0);
		srcROI2 = cvLoadImage(InputFileName, 0);
		GrayImage = cvLoadImage(InputFileName, 0);
		if(!src)
		{
			printf("Error: Couldn't open the image file.\n");
		}
	}while(!src);
	
	temp = cvCreateImage(cvSize(src->width,src->height),8,3);
	laplacian = cvCreateImage(cvSize(src->width,src->height),8,0);
		
	GrayArray = (unsigned short*)calloc(src->width*src->height,sizeof(unsigned short));
	tempGrayArray = (unsigned short*)calloc(src->width*src->height,sizeof(unsigned short));
	
	cvNamedWindow("source",1);
	cvShowImage("source",src);
	cvSetMouseCallback("source",onMouse,NULL);

	doBinaryNSharpen();

	while(src != NULL)
	{
		if(Mode == choose*2)
		{
			//cvNamedWindow("roi",1);
			cvNamedWindow("binary",1);
			cvNamedWindow("laplacian",1);
			cvNamedWindow("cvDilateNErode",1);
			cvNamedWindow("contours",1);
			cvNamedWindow("licensePlate",1);
			ROIWidth = abs(endMousePoint.x-startMousePoint.x);
			ROIHeight = abs(endMousePoint.y-startMousePoint.y);
			//~~~~~~~~~~~~~
			if(Mode == 4) {
				//cvNamedWindow("roi2",1);
				cvNamedWindow("binary2",1);
				cvNamedWindow("laplacian2",1);
				cvNamedWindow("cvDilateNErode2",1);
				cvNamedWindow("contours2",1);
				cvNamedWindow("licensePlate2",1);

				ROIWidth2 = abs(endMousePoint2.x-startMousePoint2.x);
				ROIHeight2 = abs(endMousePoint2.y-startMousePoint2.y);
			}
			//-------------------------------------------------------------------------

			cvRectangle(src, startMousePoint, endMousePoint, CV_RGB(255,0,0), 1, 8, 0);
			rectROI = cvRect(startMousePoint.x,startMousePoint.y,ROIWidth,ROIHeight);
			if(Mode == 4) {
				cvRectangle(src, startMousePoint2, endMousePoint2, CV_RGB(255,0,0), 1, 8, 0);
				rectROI2 = cvRect(startMousePoint2.x,startMousePoint2.y,ROIWidth2,ROIHeight2);}
			cvShowImage("source",src);

			cvSetImageROI(laplacian,rectROI);
			cvSetImageROI(GrayImage,rectROI);
			cvSetImageROI(srcROI,rectROI);

			//cvShowImage("roi",srcROI);
			cvShowImage("binary", GrayImage );		
			cvShowImage("laplacian", laplacian );
			//~~~~~~~~~~~~~
			if(Mode == 4) {
				laplacian2=cvCloneImage(laplacian);
				GrayImage2=cvCloneImage(GrayImage);
				srcROI2=cvCloneImage(srcROI);
				cvSetImageROI(laplacian2,rectROI2);
				cvSetImageROI(GrayImage2,rectROI2);
				cvSetImageROI(srcROI2,rectROI2);
				//cvShowImage("roi2",srcROI2);
				cvShowImage("binary2", GrayImage2);		
				cvShowImage("laplacian2", laplacian2);
			}
			//-------------------------------------------------------------------------
			
			dst = cvCreateImage( cvGetSize(srcROI), 8, 3);
			dilnEro=cvCloneImage(laplacian);
			cvDilate(dilnEro,dilnEro);  
			//cvErode(dilnEro,dilnEro);  
			cvShowImage("cvDilateNErode", dilnEro);
			if(Mode == 4) {
				dst2 = cvCreateImage( cvGetSize(srcROI2), 8, 3);
				dilnEro2=cvCloneImage(laplacian2);
				cvDilate(dilnEro2,dilnEro2);  
				//cvErode(dilnEro2,dilnEro2);  
				cvShowImage("cvDilateNErode2", dilnEro2);
			}
			//-------------------------------------------------------------------------
			
			numOfcontour = cvFindContours(dilnEro, storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);  
			cvZero(dst);
			printf("%d%s%dnumOfcontour = %d\n",dilnEro->imageSize,dilnEro->colorModel,dst->imageSize,numOfcontour);
			cvCvtColor(dilnEro,dst,CV_GRAY2BGR);  
			contours = cvCloneImage(dst);

			for( ; contour != 0; contour = contour->h_next)  
			{  
				cvDrawContours( contours, contour, color, color, -1, 1, 8 );
				cvShowImage("contours", contours);
				
				AndRect = cvBoundingRect( contour , 0 );
				//---------------------------------------------------------
				area = AndRect.width * AndRect.height;
				count = fabs( cvContourArea( contour , CV_WHOLE_SEQ ) );
				ratio = (float) AndRect.width / AndRect.height;
				
				//-----
				fRECT1.x = AndRect.x;
				fRECT1.y = AndRect.y;
				fRECT2.x = AndRect.x + AndRect.width;
				fRECT2.y = AndRect.y + AndRect.height;
				//-----
				if(area < 1000){
					cvRectangle( dst , fRECT1, fRECT2, CV_RGB(0,255,0) , 2 , 8 , 0 );
				}
				else
				{
					printf("%d,%d,area=%d,ratio=%f\n",AndRect.width,AndRect.height,area,ratio);
					cvRectangle( dst , fRECT1, fRECT2, CV_RGB(0,0,255) , 2 , 8 , 0 );
					if(area>5000)bigContour++;
				} 
				if((area>2500)&&(area<7000)&&(ratio>1.3)&&(ratio<3)){
					cvRectangle( dst , fRECT1, fRECT2, CV_RGB(255,255,0) , 2 , 8 , 0 );
					isHaveCar = true;
				}
				cvShowImage("licensePlate", dst);
			}
			if(Mode == 4) {
				numOfcontour2=cvFindContours(dilnEro2, storage2, &contour2, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);  
				cvZero(dst2);
				printf("%d%s%dnumOfcontour2 = %d\n",dilnEro2->imageSize,dilnEro2->colorModel,dst2->imageSize,numOfcontour2);
				cvCvtColor(dilnEro2,dst2,CV_GRAY2BGR);  
				contours2 = cvCloneImage(dst2);

				for( ; contour2 != 0; contour2 = contour2->h_next)  
				{  
					cvDrawContours(contours2, contour2, color, color, -1, 1, 8 );
					cvShowImage("contours2", contours2);
				
					AndRect2 = cvBoundingRect( contour2 , 0 );
					//---------------------------------------------------------
					area2 = AndRect2.width * AndRect2.height;
					count2 = fabs( cvContourArea( contour2 , CV_WHOLE_SEQ ) );
					ratio2 = (float) AndRect2.width / AndRect2.height;
					
					//-----
					sRECT1.x = AndRect2.x;
					sRECT1.y = AndRect2.y;
					sRECT2.x = AndRect2.x + AndRect2.width;
					sRECT2.y = AndRect2.y + AndRect2.height;
					//-----
					if(area2 <1000){
						cvRectangle( dst2 , sRECT1, sRECT2, CV_RGB(0,255,0) , 2 , 8 , 0 );
					}
					else
					{
						printf("%d,%d,area2=%d,ratio2=%f\n",AndRect2.width,AndRect2.height,area2,ratio2);
						cvRectangle( dst2 , sRECT1, sRECT2, CV_RGB(0,0,255) , 2 , 8 , 0 );
						if(area2>5000)bigContour2++;
					} 
					if((area2>2500)&&(area2<7000)&&(ratio2>1.3)&&(ratio2<3)){
						cvRectangle( dst2 , sRECT1, sRECT2, CV_RGB(255,255,0) , 2 , 8 , 0 );
						isHaveCar2 = true;
					}
					
					
					cvShowImage("licensePlate2", dst2);
				}
				
			}
			if(numOfcontour>25){isHaveCar = true;}
			if(numOfcontour2>25){isHaveCar2 = true;}

				if(isHaveCar){
					cvPutText(src,"YES",startMousePoint,&font,CV_RGB(0,255,0));
				}else{
					cvPutText(src,"NO",startMousePoint,&font,CV_RGB(0,255,0));
				}
				if(isHaveCar2){
					cvPutText(src,"YES",startMousePoint2,&font,CV_RGB(0,255,0));
				}else{
					cvPutText(src,"NO",startMousePoint2,&font,CV_RGB(0,255,0));
				}
				cvShowImage("source",src);
			Mode = 0;
		}
		userHitKey = cvWaitKey(1);
		
		if(userHitKey == 'x')
		{
			break;
		}
		if(userHitKey == 'q')
		{
			freeMemory();
			return main();
		}
	}
	
	freeMemory();
	return 0;
}


void onMouse(int Event, int x, int y, int flags, void *param)
{
	if(Event == CV_EVENT_LBUTTONDOWN && Mode==0)
	{
		startMousePoint.x = x;
		startMousePoint.y = y;
		Mode++;
		printf("Point1=> %d,%d\n",startMousePoint.x,startMousePoint.y);
	}
	else if (Event == CV_EVENT_LBUTTONDOWN && Mode==1)
	{
		endMousePoint.x = x;
		endMousePoint.y = y;
		Mode++;
		printf("Point2=> %d,%d\n",endMousePoint.x,endMousePoint.y);
	}
	else if(Event == CV_EVENT_LBUTTONDOWN && Mode==2)
	{
		startMousePoint2.x = x;
		startMousePoint2.y = y;
		Mode++;
		printf("Point3=> %d,%d\n",startMousePoint2.x,startMousePoint2.y);
	}
	else if (Event == CV_EVENT_LBUTTONDOWN && Mode == 3)
	{
		endMousePoint2.x = x;
		endMousePoint2.y = y;
		Mode++;
		printf("Point4=> %d,%d\n",endMousePoint2.x,endMousePoint2.y);
	}
}

void freeMemory()
{
	cvReleaseImage(&src);
	cvReleaseImage(&srcROI);
	cvReleaseImage(&dst);
	cvReleaseImage(&binarySrc);
	cvReleaseImage(&contours);
	cvReleaseImage(&GrayImage);
	cvReleaseImage(&temp);
	cvReleaseImage(&laplacian);
	cvReleaseImage(&dilnEro);
	cvReleaseImage(&srcROI2);
	cvReleaseImage(&dst2);
	cvReleaseImage(&binarySrc2);
	cvReleaseImage(&contours2);
	cvReleaseImage(&GrayImage2);
	cvReleaseImage(&laplacian2);
	cvReleaseImage(&dilnEro2);

	cvDestroyWindow("source");
	//cvDestroyWindow("roi");
	cvDestroyWindow("binary");
	cvDestroyWindow("cvDilateNErode");
	cvDestroyWindow("contours");
	cvDestroyWindow("licensePlate");
	cvDestroyWindow("laplacian");
	//cvDestroyWindow("roi2");
	cvDestroyWindow("binary2");
	cvDestroyWindow("cvDilateNErode2");
	cvDestroyWindow("contours2");
	cvDestroyWindow("licensePlate2");
	cvDestroyWindow("laplacian2");
	
	free(binarySrc);
	free(src);
	free(srcROI);
	free(dst);
	free(contours);
	free(GrayArray);
	free(tempGrayArray);
	free(GrayImage);
	free(temp);
	free(laplacian);
	free(dilnEro);
	free(binarySrc2);
	free(srcROI2);
	free(dst2);
	free(contours2);
	free(GrayImage2);
	free(laplacian2);
	free(dilnEro2);
}